// console.log("Hello World!");


// [SECTION] Functions

	// Parameters and Arguments
	
		// Functions are mostly created to create complicated tasks to run several lines of codes in succession.
		// They are also used to prevent repeating lines/blocks of codes that perform the same task/functions.


	// This function is not reusable.
	// function printInput() {
	// 	let nickname = prompt("Enter your nickname:");
	// 	console.log("Hi, " + nickname + ".");		
	// };

	// printInput();


	// This is how we demonstrate code reusability in functions.
		// name - is the function paramter
		// "Ruru", "John", and "Jane" - are the function arguments that we pass to the function paramter.
	function printName(name) {
		console.log("My name is " + name + ".");
	};

	printName("Ruru");


	printName("John");


	printName("Jane");

	// variables can also be passed as an argument.
	let sampleVar = "Johnson";

	printName(sampleVar);	


	// Important notes:
		// Function arguments cannot be used by a function if there are no function paramenters provided within the function. 


	function checkDivBy8(num) {
		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);

		let	isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}


	checkDivBy8(64);
	checkDivBy8(28);
	checkDivBy8(96);


	// Functions as Arguments
		// Function parameters can also accept other functions as agurments.
		// This is being performed in some complex functions that use other functions as argument/s to perform more complicated results.


	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.");
	};

	function invokeFunction(argumentFunction){
		argumentFunction();
	};

	// here we pass the argumerntFunction as a middleware or an argument.
	invokeFunction(argumentFunction);
	// by printing the function in the console we can get more info about it.
	console.log(argumentFunction)

	// Using multiple Parameters/Arguments
		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

	function createFullName(firstname, middleName, lastName) {
		console.log("My full name is " + firstname + " " + middleName + " " + lastName + ".");
	};
	createFullName("Juan", "Dela", "Cruz");

	// "Juan" was stored in the paramete "firsName"
	// "Dela" was stored in the paramete "middleName"
	// "Cruz" was stored in the paramete "lastName"

	createFullName("Robert", "Jones") // the "lastName" will return undefined.

	createFullName("Robert", "Jones", "Smith", "Jr.") // the "Jr" will not passed since ther are only 3 parameters.

	// Using variable as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstname, middleName, lastName);

	//Using alert()
		// alert() method allows us to show a small windows at the top of our browser page to show information to our users.

	// alert("hello world")

	// You can also use alert() to show message to the user from a later function invocation.
	function showAlertSample(){

		//ex. the user log in to his account, then the alert message will pop out after successfully logging in.
		alert("Hello, User!");
	}

	showAlertSample();

	// Notes on the use of alert()
		//Use alert() to show short message to the user.
		// Do not overuse alert() because the program/JS has to wait for it to be dismissed before continuing.

	// Using prompt()
		// prompt() allows us to show a small window at the top of the browser to gather user input.
		// The input from the prompt() will be returned as a string data type once the user dismissed the window.

		// let samplePrompt = prompt("Enter your name:");
		// console.log("hello, " + samplePrompt);
		// using the parseIn we can convert the prompt input form starting to number data type
		// let userAge = parseInt(prompt("Enter your Age: "));

		// console.log(userAge);
		// console.log(typeof userAge);

	// prompt() inside a function
	function printWelcomeMessage() {
		let firstName = prompt("Enter your First Name:");
		let lastName = prompt("Enter your Last Name:");
		let myAge = parseInt(prompt("Enter you Age:"));

		console.log("Hello, " + firstName + " " + lastName + ".");
		console.log("You have successfully registered and logged in to your account.");

		console.log("Registered account details: FirstName: " + firstName + "," + " LastName: " + lastName + "," + " Age: " + myAge + ".");
	};

	printWelcomeMessage();